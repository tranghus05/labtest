const modelCatalogs = require("../models/catalogs.js")
exports.listProduct = async function(req, res, next) {
    const listCat = await modelCatalogs
      .list()
      .then((data) => res.send(data))
      .catch((err) => res.send({ error: err })); 
}//gọi hàm trong model, để có dữ liệu từ db

exports.addProduct = async function(req, res, next) {
    let nameCat = req.body.nameCat;
    let order = req.body.order;
    let showHide = req.body.showHide;
    const addCat = await modelCatalogs
      .addProduct(nameCat, order, showHide)
      .then((data) => res.send(data))
      .catch((err) => res.send({ error: err })); 
}//gọi hàm trong model, để có dữ liệu từ db