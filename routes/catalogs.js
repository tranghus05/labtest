const express = require("express");
const { listProduct, addProduct } = require("../controllers");
const router = express.Router();
const modelCatalogs = require("../models/catalogs");

router.get("/", listProduct);

router.post("/add", addProduct);

router.post("/store", function (req, res, next) {
  //tiếp nhận dữ liệu từ form addnew để record mới vào db
  let nameCat = req.body.nameCat;
  let order = req.body.Order;
  let showShide = req.body.showShide;

  modelCatalogs.create(nameCat, order, showShide);
  res.redirect("/cat/");
});

router.get("/edit/:id", function (req, res, next) {
  //tham số id để biết record cần chỉnh
  const id = req.params.id;
  const chiTietCatalog = modelCatalogs.detail(id);
  res.render("catalog_edit", { c: chiTietCatalog });
});
router.post("/update", function (req, res, next) {
  //tiếp nhận dữ liệu từ form edit để cập nhật catalog vào db
});
router.get("/delete/:id", function (req, res) {
  //tham số id để biết record cần xóa
  const id = req.params.id;
  res.send("Xóa catalog");
});

module.exports = router;
