//Database trả về kết nối với cơ sở dữ liệu
const mysql = require('mysql');
const db = mysql.createConnection({
   host: 'localhost', 
   user: 'root', 
   password: '', 
   database: 'shop'
}); 
db.connect(function(err) {
   if (err) console.log(err.message); 
   console.log('Database is connected successfully !');
});
module.exports = db; //lệnh exports để xuất (public) ra, cho bên bên ngoài module có thể dùng được db